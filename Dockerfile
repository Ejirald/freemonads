FROM hseeberger/scala-sbt:8u212_1.2.8_2.12.8

WORKDIR /buildDir
COPY .  /buildDir
VOLUME  /var/run/docker.sock

RUN sbt 'set ( monadsWeb / Test / test ) := {}' clean assembly

# -------------------------------------------------
# -------------------------------------------------
# -------------------------------------------------

FROM openjdk:8

WORKDIR                                                             /application
COPY --from=0 /buildDir/monadsWebAsync/target/scala-2.12/monads.jar /application/

CMD ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap","-jar","monads.jar"]
