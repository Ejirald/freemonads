package monadsFlyway

import javax.sql.DataSource
import pureconfig._
import pureconfig.generic.auto._
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.MigrationVersion

object FlywayUtils {

  private final case class Config(flyway: FlywayConf)
  private final case class FlywayConf(baselineOnMigrate: Boolean,
                                      location: String,
                                      prefix: String,
                                      migrate: Boolean,
                                      migrationSeparator: String)

  def migrateDatabase(dataSource: DataSource): Flyway = {
    implicit val flywayConf: FlywayConf = loadConfigOrThrow[Config].flyway

    new Flyway() {
      setDataSource(dataSource)
      setBaselineOnMigrate(flywayConf.baselineOnMigrate)
      setBaselineVersion(MigrationVersion.EMPTY)
      setLocations(flywayConf.location)
      setSqlMigrationPrefix(flywayConf.prefix)
      setSqlMigrationSeparator(flywayConf.migrationSeparator)
      if (flywayConf.migrate) migrate()
    }
  }
}
