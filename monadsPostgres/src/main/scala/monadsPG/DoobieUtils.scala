package monadsPG

import cats.effect.{Async, ContextShift, IO}
import doobie.Transactor
import doobie.util.ExecutionContexts.fixedThreadPool
import javax.sql.DataSource
import monadsPG.DoobieTypes.ResTransactor
import monadsPG.DataSourceUtils.HickariConfig

object DoobieUtils {
  def resTransactor(ds: DataSource)
                   (implicit ev: Async[IO],
                    cs: ContextShift[IO],
                    config: HickariConfig): ResTransactor[IO] =
    for {
      ce <- fixedThreadPool[IO](config.dbConfig.connectPoolSize)
      te <- fixedThreadPool[IO](config.dbConfig.transactionPoolSize)
    } yield Transactor.fromDataSource[IO](ds, ce, te)
}
