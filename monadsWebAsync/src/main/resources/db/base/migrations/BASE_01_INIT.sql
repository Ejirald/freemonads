-- noinspection SqlNoDataSourceInspectionForFile

CREATE SCHEMA IF NOT EXISTS accounts AUTHORIZATION mayur;

DO $$
BEGIN
    CREATE SEQUENCE IF NOT EXISTS accounts.sqn_user
        AS BIGINT
        INCREMENT BY 1
        MINVALUE 1 NO MAXVALUE
        START WITH 1000
    ;

    CREATE TABLE IF NOT EXISTS accounts.user (
        id   BIGINT      PRIMARY KEY NOT NULL DEFAULT nextval('accounts.sqn_user'),
        guid VARCHAR(36)             NOT NULL
    );

    ALTER SEQUENCE accounts.sqn_user OWNED BY accounts.user.id;

END;
$$;
