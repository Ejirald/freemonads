DO $$
BEGIN
    CREATE SEQUENCE IF NOT EXISTS accounts.sqn_user_type
        AS SMALLINT
        INCREMENT BY 1
        MINVALUE 1
        NO MAXVALUE
        START WITH 10;

    CREATE TABLE IF NOT EXISTS accounts.user_type (
        id      SMALLINT    PRIMARY KEY NOT NULL DEFAULT nextval('accounts.sqn_user_type'),
        name    VARCHAR(10)             NOT NULL,
        caption VARCHAR(25)             NOT NULL
    );

    ALTER SEQUENCE accounts.sqn_user_type OWNED BY accounts.user_type.id;

    ALTER TABLE accounts.user
        ADD COLUMN u_type SMALLINT NOT NULL DEFAULT 10 REFERENCES accounts.user_type (id);

    INSERT INTO accounts.user_type (name, caption)
    VALUES ('PERSON', 'Private person'),
           ('COMPANY', 'One of the partners');
END;
$$;

DO $$
BEGIN
    CREATE SEQUENCE IF NOT EXISTS accounts.sqn_billing
        AS BIGINT
        INCREMENT BY 1
        MINVALUE 1
        NO MAXVALUE
        START WITH 1000;

    CREATE TABLE IF NOT EXISTS accounts.billing (
        id      SMALLINT    PRIMARY KEY NOT NULL DEFAULT nextval('accounts.sqn_billing'),
        user_id BIGINT                  NOT NULL,
        amount  BIGINT                  NOT NULL DEFAULT 0,
        active  BOOLEAN                 NOT NULL DEFAULT TRUE
    );

    ALTER SEQUENCE accounts.sqn_billing OWNED BY accounts.billing.id;
END;
$$