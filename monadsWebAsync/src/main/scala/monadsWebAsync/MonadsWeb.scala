package monadsWebAsync

import cats.effect.{ExitCode, IO, IOApp}
import org.http4s.server.blaze.BlazeServerBuilder
import utils.DI.{webConfig, httpApp}

object MonadsWeb extends IOApp() {

  /**
   * 0) Firs of all - entire application is endless IO-loop
   * atop of NIO, all endpoints must be present as asynchronous
   * callback on http request
   */
  override def run(args: List[String]): IO[ExitCode] =
  /**
   * 1) Resolving dependency graph via Deikstra's algorithm
   * is brilliant, but useless, scala compiler could make
   * it more efficient vay via type signature definition
   */
    BlazeServerBuilder[IO]
      .bindHttp(
        webConfig.httpConf.port,
        webConfig.httpConf.host
      ).withHttpApp(httpApp())
      .resource
      .use(_ => IO.never)
}
