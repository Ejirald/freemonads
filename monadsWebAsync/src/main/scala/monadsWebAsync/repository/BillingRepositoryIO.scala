package monadsWebAsync.repository

import doobie.free.connection.ConnectionIO
import doobie.implicits._
import monadsWebAsync.dao.Billing

trait BillingRepoAlg[M[_]] {
  def save(userId: Long): M[Billing]
}

class BillingRepositoryIO() extends BillingRepoAlg[ConnectionIO] {
  override def save(userId: Long): ConnectionIO[Billing] =
    sql"INSERT INTO accounts.billing (user_id) VALUES ($userId)"
      .update
      .withUniqueGeneratedKeys[Billing]("id", "user_id", "amount", "active")

}

