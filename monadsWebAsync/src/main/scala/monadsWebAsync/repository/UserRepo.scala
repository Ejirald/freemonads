package monadsWebAsync.repository

import cats.data.OptionT
import cats.effect.IO
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import monadsWebAsync.dao.User

/**
  * 4) Atop of each application layer we shouldn't
  * use any predefined types (as IO, ID or RestResponse),
  * any interface with default methods and fields should be
  * just an abstraction over Monoid @M[T] == ()=>T with data inside
  */
trait UserRepo[M[_]] {
  def save(user: User): M[User]
  def getByGUID(guid: String): M[Option[User]]
}

final class UserRepositoryIO extends UserRepo[ConnectionIO] {
  override def save(user: User): ConnectionIO[User] =
    sql"INSERT INTO accounts.user (guid, u_type) VALUES (${user.guid}, ${user.uType})"
      .update
      .withUniqueGeneratedKeys[User]("id", "guid", "u_type")

  override def getByGUID(guid: String): ConnectionIO[Option[User]] =
    sql"SELECT * FROM accounts.user WHERE guid = $guid"
      .query[User]
      .option

}