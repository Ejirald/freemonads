package monadsWebAsync.repository

import doobie.free.connection.ConnectionIO
import doobie.implicits._
import monadsWebAsync.dao.UserType

trait UserTypeRepo[M[_]] {
  def findAll(): M[List[UserType]]
}

final class UserTypeRepoIO extends UserTypeRepo[ConnectionIO] {
  override def findAll(): ConnectionIO[List[UserType]] =
    sql"SELECT * FROM accounts.user_type"
    .query[UserType]
    .to[List]
}
