package monadsWebAsync.routers

import cats.effect.IO
import org.http4s.dsl.io._
import org.http4s.HttpRoutes

object Health {
  final val healthController: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case GET -> Root => Ok("""{"Status":"Up"}""".stripMargin)
  }
}
