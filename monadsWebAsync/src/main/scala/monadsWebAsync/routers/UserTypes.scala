package monadsWebAsync.routers

import cats.effect.IO
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.HttpRoutes
import org.http4s.circe._
import org.http4s.dsl.io._
import monadsWebAsync.service.UserTypeAlg

object UserTypes {
  def userTypeController()(implicit uTypeService: UserTypeAlg[IO]): HttpRoutes[IO] =
    HttpRoutes.of[IO] {
      case GET -> Root / "all" =>
        uTypeService
          .findAll()
          .flatMap { res => Ok(res.asJson) }
          .handleErrorWith { err => InternalServerError(s"Cannot find user types :: ${err.getMessage}") }
    }
}
