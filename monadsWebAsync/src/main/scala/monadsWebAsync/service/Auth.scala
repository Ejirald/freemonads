package monadsWebAsync.service

import java.lang.System.currentTimeMillis
import java.util.concurrent.TimeUnit

import cats.data._
import cats.effect._
import org.http4s._
import org.http4s.dsl.io._
import org.http4s.util.CaseInsensitiveString
import org.influxdb.dto.Point

trait AuthService[M[_]] {
  def authMiddleware(service: HttpRoutes[IO]): HttpRoutes[IO]
}

final class AuthServiceIO ()
                          (implicit userService: UserService[IO],
                           monitoringService: MonitoringAlg) extends AuthService[IO] {
  private final val USER_TOKEN = CaseInsensitiveString("UserToken")

  def authMiddleware(service: HttpRoutes[IO]): HttpRoutes[IO] = Kleisli { request: Request[IO] =>
    val res: IO[Option[Response[IO]]] = EitherT.fromEither[IO] {
      request.headers.get(USER_TOKEN).toRight("Cannot find token")
    }.flatMap { header =>
      userService.findByGUID(header.value).toRight("Cannot find user")
    }.value.flatMap {
      case Left(err) => Forbidden(err)
      case Right(user) =>
        monitoringService.sendMeasure(Point.measurement("auth")
          .time(currentTimeMillis, TimeUnit.MILLISECONDS)
          .addField("id", user.id.get)
          .addField("token", user.guid)
          .build)
        Ok()
    }.map(stat => Option(stat))

    OptionT(res).flatMap {
      case Ok(__) => service(request)
      case res@Forbidden(__) => OptionT { IO { Option(res) } }
    }
  }
}
