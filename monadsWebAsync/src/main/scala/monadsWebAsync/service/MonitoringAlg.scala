package monadsWebAsync.service

import com.typesafe.scalalogging.LazyLogging
import org.influxdb.InfluxDB
import org.influxdb.dto.Point
import monadsWebAsync.utils.InfluxUtils.MaybeInfluxDB

trait MonitoringAlg {
  def sendMeasure(p: Point): Unit
}

final class MonitoringService()
                             (implicit maybeInfluxDB: MaybeInfluxDB)
  extends MonitoringAlg with LazyLogging {

  override def sendMeasure(p: Point): Unit = maybeInfluxDB match {
    case Some(iDB: InfluxDB) => iDB.write(p)
    case None => logger.info(s"Influx measure :: $p")
  }
}
