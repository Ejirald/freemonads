package monadsWebAsync.service

import cats.data.OptionT
import cats.effect.IO
import com.typesafe.scalalogging.LazyLogging
import monadsWebAsync.dao._
import doobie.free.connection.ConnectionIO
import doobie.implicits._
import monadsWebAsync.dto._
import monadsWebAsync.repository._
import monadsPG.DoobieTypes.ResTransactor

trait UserService[M[_]] {
  def create(userType: Short): M[User]
  def findByGUID(guid: String): OptionT[M, User]
  def createWithBilling(userType: Short): M[UserWithBilling]
}

/**
  * 5) Service layer should contain final operations
  * implemented as logical atomic deal - transaction
  * or any other supported fixable/revertible logic
  * with finite subset of states
  */
final class UserServiceIO()
                         (implicit val rxa: ResTransactor[IO],
                          implicit val repo: UserRepo[ConnectionIO],
                          implicit val billingRepo: BillingRepoAlg[ConnectionIO]
                         ) extends UserService[IO] with LazyLogging {

  logger.info("Initialized")

  override def create(userType: Short): IO[User] = rxa.use {
    val user = User(uType = userType.toShort)
    repo.save(user).transact(_)
  }

  override def createWithBilling(userType: Short): IO[UserWithBilling] = rxa.use {
    val user = User(uType = userType.toShort)
    (for {
      u <- repo.save(user)
      b <- billingRepo.save(u.id.get)
    } yield (u, b))
      .transact(_)
      .map(UserWithBilling(_))
  }

  override def findByGUID(guid: String): OptionT[IO, User] = OptionT {
    rxa.use {
      repo.getByGUID(guid).transact(_)
    }
  }
}
