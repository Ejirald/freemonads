package monadsWebAsync.utils

object FreeMonadsConfig {
  final case class FreeMonadsConfig(httpConf: HttpAppConf,
                                    influx: Influx)
  final case class HttpAppConf(port: Int,
                               host: String)
  final case class Influx(enabled: Boolean,
                          dbName: String,
                          url: String,
                          name: String,
                          password: String)
}
